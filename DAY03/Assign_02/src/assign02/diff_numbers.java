//5. Accept 20 integer inputs from user and print the following:
//number of positive numbers , number of negative numbers
//number of odd numbers , number of even numbers
//number of 0s.

package assign02;

import java.util.Scanner;

public class diff_numbers 
{

	public static void main(String[] args) 
	{
		int pos_num=0, neg_num=0, even=0, odd=0, zero=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number ");
		for(int i=0; i<20; i++)
		{
			int num = sc.nextInt();
			if(num % 2 == 0)
			{
				//System.out.println("Even number");
				even++;
			}
			if(num % 2 != 0 )
			{
				//System.out.println("Odd number");
				odd++;
			}
			if(num > 0)
			{
				//System.out.println("Positive number");
				pos_num++;
			}
			if(num < 0)
			{
				//System.out.println("Negative number");
				neg_num++;
			}
			if(num == 0)
			{
				//System.out.println("Zero number");
				zero++;
			}
		}
		System.out.println("Postive numbers "+pos_num);
		System.out.println("Negative numbers "+neg_num);
		System.out.println("Even numbers "+even);
		System.out.println("Odd numbers "+odd);
		System.out.println("Zero numbers "+zero);
		sc.close();
	}

}
