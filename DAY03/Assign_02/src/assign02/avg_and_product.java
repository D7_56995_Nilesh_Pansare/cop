//4. Take integer inputs from user until he/she presses q ( Ask to press q to quit 
// after every integer input ). Print average and product of all numbers.

package assign02;

import java.util.Scanner;

public class avg_and_product 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		int num;
		int prod=1, sum=0, i=0;
		char ch;
		float avg = 0;
		do 
		{
			System.out.println("Enter number");
			num = sc.nextInt();
			sum += num;
			prod *= num;
			i++;
			System.out.println("Press 'q' to exit or press other key to continue ");
			ch = sc.next().charAt(0);
		}while(ch!='q');
		System.out.println("Product of numbers is "+prod);
		avg = (float)(sum/i);
		System.out.println("Average of numbers is "+avg);
		System.out.println("You have entered "+i+" numbers");
		sc.close();
	}

}
