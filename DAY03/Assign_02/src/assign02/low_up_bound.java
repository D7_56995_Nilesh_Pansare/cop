
/*1. Program to display addition of numbers from a lower bound to an upper bound 
 using a while-loop*/

package assign02;

import java.util.Scanner;

public class low_up_bound 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Lower Bound Value ");
		int low = sc.nextInt();
		System.out.println("Enter Upper Bound Value ");
		int up = sc.nextInt();
		int sum_even= 0;
		int sum_odd = 0;
		int i = low, sum=0;
		//for(i = low; i <= up; i++)
		while(i<=up)
		{
			
			if(i % 2 == 0)
			{
				sum_even = sum_even + i;
			}
			else
			{
				sum_odd = sum_odd + i;
			}
			i++;
		}
		System.out.println("Sum of even numbers is "+sum_even);
		System.out.println("Sum of odd numbers is "+sum_odd);	
		sum = sum_even + sum_odd;
		System.out.println("Addition of all numbers is "+sum);
		sc.close();
	}
}
