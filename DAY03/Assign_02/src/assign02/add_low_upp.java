/* 2. Display sum of the odd numbers and the even numbers from a lower bound to an upper bound. 
*/

package assign02;

import java.util.Scanner;

public class add_low_upp {

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Lower Bound Value ");
		int low = sc.nextInt();
		System.out.println("Enter Upper Bound Value ");
		int up = sc.nextInt();
		int sum_even = 0;
		int sum_odd = 0;
		int i = low;
		//for(i = low; i <= up; i++)
		while(i<=up)
		{
			if(i % 2 == 0)
			{
				sum_even = sum_even + i;
			}
			else
			{
				sum_odd = sum_odd + i;
			}
			i++;
		}
		System.out.println("Sum of even numbers is "+sum_even);
		System.out.println("Sum of odd numbers is "+sum_odd);	
		sc.close();
	}
}
