
//1.2 Print the area and circumference of circle, given its radius.
package nilesh;

import java.util.Scanner;

public class circle_area_circum 
{
	public static void main(String[] args) 
	{
		float r;
		double area, circum;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of radius = ");
		r = sc.nextFloat();
		area = 3.14 * r * r;
		System.out.println("Area of Circle is = "+area);
		circum = 2 * 3.14 * r;
		System.out.println("Circumference of circle is = "+circum);
		sc.close();
	}
}

/*public class circle_area_circum
{
	public static void main(String args[])
	{
		float r = Float.parseFloat(args[0]);
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value of radius");
		r = sc.nextFloat();
		System.out.println("Area of Circle is =  "+(3.14 * r * r));
		System.out.println("Circumference of circle is =  "+(2 * 3.14 * r));
		sc.close();
	}
}*/
