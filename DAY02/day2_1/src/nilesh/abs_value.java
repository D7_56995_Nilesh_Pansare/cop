//1.6 Write a program to print absolute value entered by user.
//Eg. INPUT : 1       OUTPUT : 1
//	  INPUT : -1      OUTPUT : 1

package nilesh;

import java.util.Scanner;
public class abs_value 
{
	public static void main(String[] args) 
	{
		System.out.println("Enter the number ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		//while(num!=0)
		{	if(num>0)
				System.out.println(num);
			else
				System.out.println(-(num));
		}
		sc.close();
	}

}
