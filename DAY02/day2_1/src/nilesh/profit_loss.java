//1.7 A shop will give discount of 10% if the cost of purchased quantity is more than 1000.
//Ask user for quantity and suppose, one unit will cost 100. Calculate and print total cost for user.

package nilesh;

import java.util.Scanner;

public class profit_loss 
{
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int quantity;
		int totalPrice;
		int discount;
		Scanner sc = new Scanner(System.in);
		System.out.println("Please, enter quantity of units : ");
		quantity = sc.nextInt();
		totalPrice = quantity * 100;
		if(quantity>1000)
		{
			System.out.println("Congratulations,you will get 10% discount!!!");
			discount = totalPrice /10;
			System.out.println("Discount = " + discount);
			totalPrice -= discount;
			System.out.println("Total Price = " + totalPrice);
		}
		else
		{
			System.out.println("Total Price = " + totalPrice);
		}
		
		sc.close();
	}
}
	
	
	
	/*public static void main(String[] args) 
	{
		int mp = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("---- Ganesh Mall ----");
		System.out.println("Our mall offered 10% discount on above Rs.1000 shopping.");
		System.out.println("Enter the quantity ");
		int qty = sc.nextInt();
		int cost_price = qty * 100;
		System.out.println("The total bill is "+cost_price);
		if(cost_price > 1000)
		{
			cost_price = (cost_price * 10) / 100;
			mp = mp - cost_price;
			System.out.println("The final price after getting discount is "+mp);
		}
		System.out.println("Your cost price is "+cost_price);
		sc.close();
	}
*/
