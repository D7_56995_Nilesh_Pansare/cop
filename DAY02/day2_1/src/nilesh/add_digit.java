
//1.1 Write a Java program that reads an integer between 0 to 1000 and adds all the digits in the integer.
package nilesh;
import java.util.Scanner;
public class add_digit 
{
	public static void main(String[] args)
	{
		int num;
		int sum = 0, rem = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number ");
		num = sc.nextInt();
		if(num>0 && num<1000)
		{
			while(num>0)
			{
				rem = num % 10;
				sum = sum + rem;
				num = num / 10;
			}
			System.out.println("Addition of the digit is =  "+sum);
			sc.close();
		}
		else
		{
			System.out.println("You do not have enter number between 1 to 1000...");
		}
	}
}
