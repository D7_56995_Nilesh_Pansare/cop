/*2. Write a program that would print the information (name, year of joining, salary, address) of three employees by creating a class named 
'Employee'. The output should be as follows:
Name Year of joining Address
EMP1 	2000 		  Pune
EMP2 	2002 	 	  Pune
EMP3 	2008 		  Mumbai
*/

package assign;

import java.util.Scanner;

class Employee
{
	private int doj;
	private String name, address;
	private float salary;
	
	Employee()
	{
		
	}
	
	Employee(String nm, int dj, String add)
	{
		this.name=nm;
		this.doj=dj;
		//this.salary=sal;
		this.address=add;
	}

	@Override
	public String toString() {
		return "Employee [doj=" + doj + ", name=" + name + ", address=" + address + "]";
	}
	
	
}
public class Assign3_2 
{
	//Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
	Employee[] e=new Employee[3];
		for(int i=0; i<3; i++)
		{
			e[i]=new Employee();
		}
		
		Employee e3=new Employee("EMP 1",2000, "Pune");
		System.out.println(e3.toString());
		Employee e1=new Employee("EMP 2", 2002, "Pune");
		System.out.println(e1.toString());
		Employee e2=new Employee("EMP 3", 2008, "Mumbai");
		System.out.println(e2.toString());
	}

}
