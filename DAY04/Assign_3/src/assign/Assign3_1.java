/*1. Write a program to print the area of two rectangles having sides (4,5) and (5,8) respectively by creating a class named 'Rectangle' 
 with a method named 'Area' which returns the area and length and breadth passed as parameters to its constructor.
*/

package assign;

class Rectangle
{	
	private int len;
	private int breadth;
	
	Rectangle(int l, int b)
	{
		this.len=l;
		this.breadth=b;
	}
	int area()
	{
		return(len*breadth);
	}
}

public class Assign3_1 
{
	public static void main(String[] args) 
	{
		int area=0;
		System.out.println();
		Rectangle r=new Rectangle(4,5);
		area=r.area();
		System.out.println("Area of Rectangle is "+area);
		System.out.println();
		Rectangle r1=new Rectangle(5,8);
		area=r1.area();
		System.out.println("Area of Rectangle is "+area);
	}
}
