package com.assignment1;
import java.util.*;
public class Assgn1_01 {

	public static void main(String[] args)
	{
		int num;
		int rem=0,sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number:");
		num=sc.nextInt();
		if(num<1000&&num>0)
		{
		while(num>0)
		{
			rem=num%10;
			sum=sum+rem;
			num=num/10;
		}
		
		System.out.println("Addition = "+sum);
		sc.close();
		}
		else
		{
			System.out.println("Number must be from 0 to 1000");
		}
	}

}
